// -*- c++ -*-
/*
  DVDfs - mount a DVD using libdvdread (and libdvdcss, if installed)
  Copyright (C) 2009  Katie Rust (ktpanda)

  Based on fusexmp_fh template

  This program can be distributed under the terms of the GNU GPL.
  See the file COPYING.
*/

#ifndef _UTIL_H
#define _UTIL_H
using namespace std;


template<typename RefType>
class sptr {
protected:
    RefType* objref;

    inline void set_ref(RefType* obj) throw() {
        if (obj == objref)
            return;

	RefType* old = objref;

	if (obj)
            obj->ref();

	objref = obj;

	if (old)
            old->deref();
    }


public:
    sptr(RefType* ref = NULL) throw() : objref(NULL) { *this = ref; }
    sptr(const sptr& ref) throw() : objref(NULL) { *this = ref; }

    sptr& operator=(RefType* ref) throw() { set_ref(ref); return *this; }
    sptr& operator=(const sptr& ref) throw() { set_ref(ref.objref); return *this; }

    sptr& swap(sptr& o) throw() {
        RefType* tmp = objref;
        objref = o.objref;
        o.objref = tmp;
        return *this;
    }
    ~sptr() { set_ref(NULL); }

    // Allow assignment to sptr<BaseClass> from sptr<SubClass>.

    template<typename newtp>
    sptr(const sptr<newtp>& ref) throw() : objref(NULL) { *this = &*ref; }

    template<typename newtp>
    sptr& operator=(const sptr<newtp>& ref) throw() { return (*this = &*ref); }


    // Dynamically or statically cast the pointer to another
    // type. call as ptr.[stat]cast<NewType>()

    template<typename newtp>
    sptr<newtp> statcast() const throw() {
	return static_cast<newtp*>(objref);
    }

    template<typename newtp>
    sptr<newtp> cast() const throw() {
	return dynamic_cast<newtp*>(objref);
    }

    // get the object that this pointer refers to.
    RefType& operator*() const throw() { return *objref; }
    RefType* operator->() const throw() { return objref; }
    operator RefType*() const throw() { return objref; }
};

class Refcount {
    int refcount;

protected:
    void makestatic() { refcount = -1; }

public:
    Refcount() : refcount(0) { }
    virtual ~Refcount() { }

    inline void ref() {
        if (refcount == -1)
            return;
        ++refcount;
    }
    inline void deref() {
        if (refcount == -1)
            return;
        if (!--refcount)
            delete this;
    }
};

#endif
