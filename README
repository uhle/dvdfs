DVDfs (C) 2009 Katie Rust (ktpanda)
-----------------------------------

DVDfs is a userspace filesystem which mounts a DVD using libdvdread
(and, by extension, libdvdcss). You can use this to make an exact copy
of the DVD video file structure.

Since libdvdread only reads the VIDEO_TS/ directory, any other
directories and files on the disk will NOT be visible in the mounted
filesystem. However, since they are not encrypted, they can be
retrieved by mounting the disk normally as a UDF filesystem.

Usage
----------------------------------------

dvdfs -o [options] mount point

DVDfs accepts all standard FUSE options as well as:

-o device=DEVICE		Use DEVICE instead of /dev/dvd
-o includebups			Include BUP files in filesystem. These
				are an exact copy of the corresponding
				IFO files.
-o dynamic			Create a special file called /.action
				which can be used to close and re-open
				the device:

				echo open > .action
				echo close > .action

Installation
----------------------------------------

DVDfs requires libdvdread and FUSE. libdvdcss is recommended, but not
required. libdvdread will dynamically load libdvdcss at runtime.

libdvdread and libdvdcss: http://www.dtek.chalmers.se/groups/dvd/downloads.shtml
FUSE: http://fuse.sourceforge.net/

DVDfs is so small I didn't bother with a configure script. To compile
it, just run "make".
