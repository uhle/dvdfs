OBJS=fusevfs.o dvdfs.o

.PHONY: all install clean

all: dvdfs

install:
	cp dvdfs /usr/bin

clean:
	rm -f $(OBJS) dvdfs

%.o: %.cpp fusevfs.h util.h
	g++ -O2 -Wall `pkg-config fuse --cflags` -DFUSE_USE_VERSION=25 -o $@ -c $<

dvdfs: $(OBJS)
	g++ -o dvdfs $(OBJS) `pkg-config fuse --libs` -ldvdread

